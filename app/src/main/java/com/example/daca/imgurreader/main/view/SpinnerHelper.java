package com.example.daca.imgurreader.main.view;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.MainActivity;
import com.example.daca.imgurreader.main.ObserverBuilder;
import com.example.daca.imgurreader.main.presenter.Presenter;
import com.example.daca.imgurreader.util.Url;
import com.example.daca.imgurreader.util.VolleyLoader;

import java.util.ArrayList;

/**
 * Created by Daca on 22.04.2017..
 */

/*   Sets up adapter for spinner, arrayList with items,
 *   adapter for items, spinner color scheme
 *   handles onItemSelected for individual items
 */
public class SpinnerHelper implements AdapterView.OnItemSelectedListener {

    private MainActivity activity;
    private Spinner spinner;

    public SpinnerHelper(MainActivity activity) {
        this.activity = activity;
        spinner = (Spinner) activity.findViewById(R.id.toolbar_spinner);
    }

    public void setupSpinner(){

        ArrayList<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Most Viral - popular");
        spinnerArray.add("Most Viral - newest");
        spinnerArray.add("User Submitted - popular");
        spinnerArray.add("User Submitted - newest");
        spinnerArray.add("Saved");
        spinnerArray.add("Tag");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();
        activity.presenter.spinnerHelperItemClick(selectedItem);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
