package com.example.daca.imgurreader.main.view;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.MainActivity;
import com.example.daca.imgurreader.main.ObserverBuilder;
import com.example.daca.imgurreader.main.entities.Post;
import com.example.daca.imgurreader.main.view.recyclerview.EndlessRecyclerOnScrollListener;
import com.example.daca.imgurreader.main.view.recyclerview.RvAdapter;
import com.example.daca.imgurreader.util.LayoutCollectons;
import com.example.daca.imgurreader.util.VolleyLoader;

import java.util.ArrayList;

/**
 * Created by ttlnisoffice on 7/20/17.
 */

public class RecyclerHelper {

    RecyclerView rv;
    LinearLayoutManager manager;
    MainActivity activity;
    private RvAdapter adapter;
    SharedPreferences preferences;

    boolean loadMore = false;
    private ArrayList<Post> list;

    RecyclerView drawerRv;

    public RecyclerHelper(RecyclerView rv, MainActivity activity) {
        this.rv = rv;
        this.activity = activity;
        manager = new LinearLayoutManager(activity);
        rv.setLayoutManager(manager);
        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        String url = preferences.getString("url", null);
        if (url != null && !url.contains("no")){
            setupScrollListener(url);
        }

    }

    private void setupScrollListener(final String url){
        rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int currentPage) {
                loadMore = true;
                Toast.makeText(activity, "load more", Toast.LENGTH_SHORT).show();

                new VolleyLoader(activity).loadWithToken(
                        new ObserverBuilder(activity).getPopularObserver(),
                        url,
                        "loadPopular", false);
            }
        });

    }

    public void setupRvAdapter(ArrayList<Post> arrayList) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        int layout = 0;
        int x = preferences.getInt("LAYOUT", 0);
        layout = new LayoutCollectons().getLayout(x);
        if (loadMore) {
            loadMore = false;
            for (int i = 0; i < arrayList.size(); i++) {
                list.add(arrayList.get(i));
            }
            adapter.notifyDataSetChanged();
        } else {
            list = arrayList;
            adapter = new RvAdapter(list, activity, layout);
            rv.setAdapter(adapter);
        }
    }

    public void resetRv(){
        rv = null;
        rv = (RecyclerView) activity.findViewById(R.id.rv_main);
        String url = preferences.getString("url", null);
        if (url != null && !url.contains("no")){
            setupScrollListener(url);
        }
    }

    public void resetAdapter(int layout){
        adapter = null;
        adapter = new RvAdapter(list, activity, layout);
        rv.setAdapter(adapter);
    }


    public void LoadDrawerItems() {
        new VolleyLoader(activity).loadWithToken(
                new ObserverBuilder(activity).getDrawerObserver(),
                "https://api.imgur.com/3/tags",
                "loadTags",
                false);
    }

    public void setupDrawerAdapter(ArrayList<String> arrayList){
        com.example.daca.imgurreader.main.view.drawer_rv.RvAdapter adapter =
                new com.example.daca.imgurreader.main.view.drawer_rv.RvAdapter(activity, arrayList);
        drawerRv = (RecyclerView) activity.findViewById(R.id.drawer_rv);
        drawerRv.setLayoutManager(new LinearLayoutManager(activity));
        drawerRv.setAdapter(adapter);
    }
}
