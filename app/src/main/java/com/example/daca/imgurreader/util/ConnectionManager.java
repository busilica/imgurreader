package com.example.daca.imgurreader.util;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Daca on 20.03.2017..
 */

public class ConnectionManager {

    private static RequestQueue queue;

    public static RequestQueue getInstance(Context context){
        if (queue == null){
            queue = Volley.newRequestQueue(context);
        }
        return queue;
    }
}
