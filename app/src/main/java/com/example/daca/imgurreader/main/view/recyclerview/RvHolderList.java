package com.example.daca.imgurreader.main.view.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 19.04.2017..
 */

public class RvHolderList extends RvHolder {

    TextView title;
    TextView ups;
    ImageView imageView;
    ProgressBar progressBar;

    public RvHolderList(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txt_title_custom_row_main_list);
        ups = (TextView) itemView.findViewById(R.id.txt_ups_custom_row_main_list);
        imageView = (ImageView) itemView.findViewById(R.id.image_custom_row_main_list);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar_list);
    }
}
