package com.example.daca.imgurreader.util;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 22.04.2017..
 */

public class LayoutCollectons {

    private static final int CARDS = R.layout.custom_row_main;
    private static final int CARDS2 = R.layout.custom_row_main_cards2;
    private static final int SMALLCARDS = R.layout.custom_row_main_small_card;
    private static final int LIST = R.layout.custom_row_main_list;
    private static final int BLANK = R.layout.custom_row_main_blank;

    public int getLayout(int x){
        switch (x) {
            case 0:
                return LayoutCollectons.getCARDS();
            case 1:
                return LayoutCollectons.getCARDS2();
            case 2:
                return LayoutCollectons.getSMALLCARDS();
            case 3:
                return LayoutCollectons.getLIST();
            case 4:
                return LayoutCollectons.getBLANK();
            default:
                return LayoutCollectons.getCARDS();
        }
    }

    public static int getCARDS() {
        return CARDS;
    }

    public static int getCARDS2() {
        return CARDS2;
    }

    public static int getSMALLCARDS() {
        return SMALLCARDS;
    }

    public static int getLIST() {
        return LIST;
    }

    public static int getBLANK() {
        return BLANK;
    }
}
