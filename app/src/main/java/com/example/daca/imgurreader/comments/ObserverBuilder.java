package com.example.daca.imgurreader.comments;

import com.example.daca.imgurreader.comments.entities.Comment;
import com.example.daca.imgurreader.comments.entities.Image;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 15.04.2017..
 */

public class ObserverBuilder {

    private CommentsActivity activity;

    public ObserverBuilder(CommentsActivity activity) {
        this.activity = activity;
    }

    public Observer<ArrayList<Comment>> getObserver(){
        Observer<ArrayList<Comment>> observer = new Observer<ArrayList<Comment>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Comment> value) {

                activity.setupAdapter(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        return observer;
    }

    public Observer<ArrayList<Image>> getImageObserver() {
        Observer<ArrayList<Image>> observer = new Observer<ArrayList<Image>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Image> value) {
                activity.setImagesList(value);
                activity.loadComments();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        return observer;
    }
}
