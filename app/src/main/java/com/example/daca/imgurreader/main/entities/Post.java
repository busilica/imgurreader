package com.example.daca.imgurreader.main.entities;

/**
 * Created by Daca on 14.04.2017..
 */

public class Post {

    private String id;
    private String title;
    private String author;
    private String cover;
    private String gifCover;
    private String gif;
    private String ups;
    private String downs;
    private String coverHeight;
    private boolean favorite;

    public Post() {

    }

    public Post(String id, String title, String author, String cover, String gifCover, String gif, String ups,
                String downs, String coverHeight, boolean favorite) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.cover = cover;
        this.gifCover = gifCover;
        this.gif = gif;
        this.ups = ups;
        this.downs = downs;
        this.coverHeight = coverHeight;
        this.favorite = favorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getGifCover() {
        return gifCover;
    }

    public void setGifCover(String author) {
        this.gifCover = gifCover;
    }

    public String getGif() {
        return gif;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    public String getUps() {
        return ups;
    }

    public void setUps(String ups) {
        this.ups = ups;
    }

    public String getDowns() {
        return downs;
    }

    public void setDowns(String downs) {
        this.downs = downs;
    }

    public String getCoverHeight() {
        return coverHeight;
    }

    public void setCoverHeight(String coverHeight) {
        this.coverHeight = coverHeight;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
