package com.example.daca.imgurreader;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Daca on 21.04.2017..
 */

public class App extends Application {

    private int theme = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        //we load the Night Mode state here
        //this will be launched before everything else
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.theme = preferences.getInt("THEME", 0);
    }

    public int whichThemeIsSelected(){
        return theme;
    }

    public void setTheme(int isNightModeEnabled){
        this.theme = isNightModeEnabled;
    }


}
