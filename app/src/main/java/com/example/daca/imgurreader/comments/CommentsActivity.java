package com.example.daca.imgurreader.comments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.comments.entities.Comment;
import com.example.daca.imgurreader.comments.entities.Image;
import com.example.daca.imgurreader.util.Url;
import com.example.daca.imgurreader.util.VolleyLoader;
import com.example.daca.imgurreader.comments.recyclerview.RvAdapter;



import java.util.ArrayList;

public class CommentsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RvAdapter adapter;
    ObserverBuilder observerBuilder;
    String imageUrl;
    String commentUrl;
    ArrayList<Image> imagesList;

    TextView title;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this code must be before onCreate
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int theme = preferences.getInt("THEME", R.id.theme_light);
        switch (theme){
            case R.id.theme_light:
                setTheme(R.style.ActivityThemeLight);
                break;
            case R.id.theme_dark:
                setTheme(R.style.ActivityThemeDark);
                break;
            case R.id.theme_sol_light:
                setTheme(R.style.ActivityThemeSolarizedLight);
                break;
            case R.id.theme_night_blue:
                setTheme(R.style.ActivityThemeNightBlue);
                break;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        intent = getIntent();

        title = (TextView) findViewById(R.id.title_comments_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_comments);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(intent.getStringExtra("title"));
        //toolbar.setTitle(intent.getStringExtra("title"));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.rv_comments_activity);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        observerBuilder = new ObserverBuilder(this);
        String id = intent.getStringExtra("id");
        Url url = new Url(this);

        commentUrl = url.getCommentUrlFirstPart() + id + url.getCommentUrlSecondPart();
        imageUrl = url.getImageUrlFirstPart() + id + url.getImageUrlSecondPart();

        new VolleyLoader(this).loadWithToken(observerBuilder.getImageObserver(), imageUrl, "loadImages", false);
    }

    public void setImagesList(ArrayList<Image> list){
        imagesList = list;
    }

    public void loadComments() {
        new VolleyLoader(this).loadWithToken(observerBuilder.getObserver(), commentUrl, "loadComments", false);
    }

    public void setupAdapter(ArrayList<Comment> comments){
        adapter = new RvAdapter(comments, imagesList, this);
        recyclerView.setAdapter(adapter);
    }

    public void addReplies(CardView card, int index){
        recyclerView.addView(card, index);
    }


}
