package com.example.daca.imgurreader.main.view.drawer_rv;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by ttlnisoffice on 8/22/17.
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView tag;

    public RvHolder(View itemView) {
        super(itemView);
        tag = (TextView) itemView.findViewById(R.id.txt_drawer_custom_row);
    }
}
