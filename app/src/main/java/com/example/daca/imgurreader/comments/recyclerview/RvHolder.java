package com.example.daca.imgurreader.comments.recyclerview;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 16.04.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView comment;
    TextView comment2;
    ImageView image;
    TextView numOfReplies;
    CardView card;
    TextView author;

    public RvHolder(View itemView) {
        super(itemView);
        comment = (TextView) itemView.findViewById(R.id.txt_comment_custom_row_comments);
        comment2 = (TextView) itemView.findViewById(R.id.txt_comment_custom_row_comments_2);
        image = (ImageView) itemView.findViewById(R.id.image_custom_row_comments);
        numOfReplies = (TextView) itemView.findViewById(R.id.txt_num_of_replies_custom_row_comments);
        card = (CardView) itemView.findViewById(R.id.card_custom_row_comments);
        author = (TextView) itemView.findViewById(R.id.txt_comment_custom_row_author);
    }
}
