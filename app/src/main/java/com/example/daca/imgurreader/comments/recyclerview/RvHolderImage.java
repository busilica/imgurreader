package com.example.daca.imgurreader.comments.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 18.04.2017..
 */

public class RvHolderImage extends RvHolder {

    ImageView image;
    TextView description;

    public RvHolderImage(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.image_custom_row_comments);
        description = (TextView) itemView.findViewById(R.id.txt_description_custom_row_comments);
    }
}
