package com.example.daca.imgurreader.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Daca on 27.03.2017..
 */

public class VolleyLoader {

    private RequestQueue            queue;
    private Context                 context;
    private SharedPreferences       preferences;
    private SharedPreferences.Editor editor;
    private Response.ErrorListener  errorListener;
    private final String            token;

    public VolleyLoader(Context c) {
        context         = c;
        preferences     = PreferenceManager.getDefaultSharedPreferences(context);
        editor          = preferences.edit();
        token           = getToken();
        errorListener   = setupErrorListener();

    }

    public void loadWithToken(final Observer observer, final String url, final String cmd, boolean shouldRefreshToken){

        refreshToken();

       /*if (shouldRefreshToken){
           refreshToken();
       } else {*/
           queue = ConnectionManager.getInstance(context);

           Response.Listener<String> listener = new Response.Listener<String>() {
               @Override
               public void onResponse(String response) {
                   Observable<ArrayList> observable = Observable.just(response)
                           .map(new Function<String, ArrayList>() {
                               @Override
                               public ArrayList apply(String s) throws Exception {
                                   switch (cmd){
                                       case "loadPopular":
                                           return new JsonParser().parsePopular(s);
                                       case "loadComments":
                                           return new JsonParser().parseComments(s);
                                       case "loadImages":
                                           return new JsonParser().parseImages(s);
                                       case "loadTags":
                                           return new JsonParser().parseTags(s);
                                       case "loadTagItems":
                                           return new JsonParser().parseTagItems(s);
                                       default:
                                           return null;
                                   }

                               }
                           })
                           .subscribeOn(Schedulers.newThread())
                           .observeOn(AndroidSchedulers.mainThread());

                   observable.subscribe(observer);
               }
           };

           StringRequest request = null;

           String isLoggedIn = preferences.getString("isLoggedIn", null);

           if (isLoggedIn != null && isLoggedIn.equals("yes") && token != null) {
               request = new StringRequest(Request.Method.GET, url, listener, errorListener){
                   @Override
                   public Map<String, String> getHeaders() throws AuthFailureError {
                       Map<String, String> headers = new HashMap<String, String>();
                       headers.put("Authorization", "Bearer " + token);
                       headers.put("User-Agent", "Imgur Reader");
                       return headers;
                   }
               };
           } else {
               request = new StringRequest(Request.Method.GET, url, listener, errorListener){
                   @Override
                   public Map<String, String> getHeaders() throws AuthFailureError {
                       Map<String, String> headers = new HashMap<String, String>();
                       headers.put("Authorization", "Client-ID a3855211c361c46");
                       headers.put("User-Agent", "Imgur Reader");
                       return headers;
                   }
               };
           }

           queue.add(request);
       // }
    }

    private void refreshToken() {
        final String refreshToken = preferences.getString("REFRESH_TOKEN", null);
        //refresh

        String url3 = "https://api.imgur.com/oauth2/token?grant_type=refresh_token&client_id=a3855211c361c46&client_secret=e6108fc041ec02de7437d26049d737a6af90ddc8&refresh_token="+refreshToken;
        String url = "https://api.imgur.com/oauth2/token?refresh_token="+refreshToken+"&client_id=a3855211c361c46&client_secret=e6108fc041ec02de7437d26049d737a6af90ddc8&grant_type=refresh_token";
        String url2 = "https://api.imgur.com/oauth2/token";
        queue = ConnectionManager.getInstance(context);
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String s = response;
                String test = "";

            }
        };

        StringRequest request = new StringRequest(Request.Method.POST, url, listener, errorListener){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + token);
                headers.put("User-Agent", "Imgur Reader");
                return headers;
            }
        };

        /*StringRequest request = new StringRequest(Request.Method.GET, url, listener, errorListener){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Client-ID a3855211c361c46");
                headers.put("User-Agent", "Imgur Reader");
                return headers;
            }
        };*/

        queue.add(request);



        /*
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("refresh_token", refreshToken);
                headers.put("client_id", "a3855211c361c46");
                headers.put("client_secret", "e6108fc041ec02de7437d26049d737a6af90ddc8");
                headers.put("grant_type", "refresh_token");
                return headers;
            }
        };
        queue.add(request);
        */
    }

    public void userAction(String id, String cmd){

        queue = ConnectionManager.getInstance(context);

        SharedPreferences.Editor editor = preferences.edit();
        final boolean veto = preferences.getBoolean(id, false);

        String url   = "";
        String toast = "";

        switch (cmd) {
            case "favoriteAlbum":
                if (veto){
                    url = "https://api.imgur.com/3/album/"   + id + "/favorite";
                    //editor.remove(id);
                   /* url = "https://api.imgur.com/3/album/"   + id + "/favorite";
                    toast = "Saved";
                    editor.putString(id, id);*/
                } else {
                    url = "https://api.imgur.com/3/album/"   + id + "/favorite";
                    toast = "Saved";
                    editor.putBoolean(id, true);
                }
                editor.apply();
                break;
            case "favoriteImage":
                url = "https://api.imgur.com/3/image/" + id + "/favorite";
                break;
            case "upVote":
                if (veto){
                    url = "https://api.imgur.com/3/gallery/" + id + "/vote/veto";
                    editor.remove(id);
                } else {
                    url = "https://api.imgur.com/3/gallery/" + id + "/vote/up";
                    toast = "Voted";
                    editor.putBoolean(id, true);
                }
                editor.apply();
                break;
            case "downVote":
                if (veto) {
                    url = "https://api.imgur.com/3/gallery/" + id + "/vote/veto";
                    editor.remove(id);
                }else {
                    url = "https://api.imgur.com/3/gallery/" + id + "/vote/up";
                    toast = "Voted";
                    editor.putBoolean(id, true);
                }
                editor.apply();
                break;
        }

        final String finalToast = toast;
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        Toast.makeText(context, finalToast, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        StringRequest request = new StringRequest(Request.Method.POST, url, listener, errorListener){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + token);
                headers.put("User-Agent", "Imgur Reader");
                return headers;
            }
        };

        queue.add(request);
    }

    private String getToken() {

        String isLoggedIn = preferences.getString("isLoggedIn", null);

        if (isLoggedIn != null && isLoggedIn.equals("yes")){
            return preferences.getString("TOKEN", null);
        } else {
            return null;
        }
    }

    private Response.ErrorListener setupErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error => " , error.toString());

                /*if (error.toString().contains("AuthFailureError")){
                    editor.putString("isLoggedIn", "no");
                }*/

            }
        };
    }

}
