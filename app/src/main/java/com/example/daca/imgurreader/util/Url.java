package com.example.daca.imgurreader.util;

import android.content.Context;

import com.github.fernandodev.androidproperties.lib.AssetsProperties;
import com.github.fernandodev.androidproperties.lib.Property;

/**
 * Created by ttlnisoffice on 7/13/17.
 */

public class Url extends AssetsProperties {

    public Url(Context context) {
        super(context);
    }

    /********** Main ******************************************************************************/

    @Property("userHot")
    String userHot;

    @Property("userNew")
    String userNew;

    @Property("viralHot")
    String viralHot;

    @Property("viralNew")
    String viralNew;

    /********** Comments **************************************************************************/

    @Property("commentUrlFirstPart")
    String commentUrlFirstPart;

    @Property("commentUrlSecondPart")
    String commentUrlSecondPart;

    @Property("imageUrlFirstPart")
    String imageUrlFirstPart;

    @Property("imageUrlSecondPart")
    String imageUrlSecondPart;

    /********** Login *****************************************************************************/

    @Property("loginUrl")
    String loginUrl;

    public String getUserHot() {
        return userHot;
    }

    public void setUserHot(String userHot) {
        this.userHot = userHot;
    }

    public String getUserNew() {
        return userNew;
    }

    public void setUserNew(String userNew) {
        this.userNew = userNew;
    }

    public String getViralHot() {
        return viralHot;
    }

    public void setViralHot(String viralHot) {
        this.viralHot = viralHot;
    }

    public String getViralNew() {
        return viralNew;
    }

    public void setViralNew(String viralNew) {
        this.viralNew = viralNew;
    }

    public String getCommentUrlFirstPart() {
        return commentUrlFirstPart;
    }

    public void setCommentUrlFirstPart(String commentUrlFirstPart) {
        this.commentUrlFirstPart = commentUrlFirstPart;
    }

    public String getCommentUrlSecondPart() {
        return commentUrlSecondPart;
    }

    public void setCommentUrlSecondPart(String commentUrlSecondPart) {
        this.commentUrlSecondPart = commentUrlSecondPart;
    }

    public String getImageUrlFirstPart() {
        return imageUrlFirstPart;
    }

    public void setImageUrlFirstPart(String imageUrlFirstPart) {
        this.imageUrlFirstPart = imageUrlFirstPart;
    }

    public String getImageUrlSecondPart() {
        return imageUrlSecondPart;
    }

    public void setImageUrlSecondPart(String imageUrlSecondPart) {
        this.imageUrlSecondPart = imageUrlSecondPart;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }
}
