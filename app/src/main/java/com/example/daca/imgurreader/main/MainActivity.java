package com.example.daca.imgurreader.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.entities.Post;
import com.example.daca.imgurreader.main.presenter.Presenter;
import com.example.daca.imgurreader.main.view.OptionsActivity;
import com.example.daca.imgurreader.main.view.RecyclerHelper;
import com.example.daca.imgurreader.main.view.SpinnerHelper;
import com.example.daca.imgurreader.main.view.recyclerview.EndlessRecyclerOnScrollListener;
import com.example.daca.imgurreader.main.view.recyclerview.RvAdapter;
import com.example.daca.imgurreader.util.LayoutCollectons;
import com.example.daca.imgurreader.util.VolleyLoader;


import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public RecyclerView recyclerView;

    static int layout;

    public RecyclerHelper recyclerHelper;

    public Presenter presenter;

    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this code must be before onCreate
        presenter = new Presenter(this);
        presenter.changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_activity_main);
         toggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_closed);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new SpinnerHelper(this).setupSpinner();

        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerHelper = new RecyclerHelper(recyclerView, this);

        layout = R.layout.custom_row_main;

        String code2 = "http://www.example.com/my_redirect#access_token=2a6a46c429f7d5a9c2d3150979d8576ec24b5dcf&expires_in=2419200&token_type=bearer&refresh_token=7628eb12b21f46db45a1c60f82bf11bc171cc591&account_username=busilica123&account_id=66204775";
        String code3 = "http://www.example.com/my_redirect#access_token=2a6a46c429f7d5a9c2d3150979d8576ec24b5dcf&expires_in=2419200&token_type=bearer&refresh_token=7628eb12b21f46db45a1c60f82bf11bc171cc591&";
        /*int start = 140;
        int end = 180;
        String code4 = code2.substring(start, end);*/


    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.log_in) {
            presenter.loginUser();
        }

        if (item.getGroupId() == R.id.theme_group) {
            presenter.saveTheme(item.getItemId());
            presenter.changeTheme();
            presenter.restartActivity();
        }

        if (toggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /*
    @Override
    public void changeLayout(int layout) {
        recyclerHelper.resetAdapter(layout);
    }
    */
}
