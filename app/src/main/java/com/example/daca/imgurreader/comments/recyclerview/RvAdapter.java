package com.example.daca.imgurreader.comments.recyclerview;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.bumptech.glide.Glide;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.comments.entities.Comment;
import com.example.daca.imgurreader.comments.CommentsActivity;
import com.example.daca.imgurreader.comments.entities.Image;
import com.example.daca.imgurreader.util.VolleyLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Daca on 16.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> implements EasyVideoCallback {

    private ArrayList<Comment> list;
    private ArrayList<Image> imageLinks;
    private CommentsActivity activity;
    private LayoutInflater inflater;

    private Comment com;

    private static final int VIEW_IMAGE = 0;
    private static final int VIEW_COMMENT = 1;
    private static final int VIEW_GIF = 2;

    public RvAdapter(ArrayList<Comment> list, ArrayList<Image> imageLinks, CommentsActivity activity) {
        this.list = list;
        this.imageLinks = imageLinks;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
        Log.d("commentsSize =>", String.valueOf(list.size()));
        Log.d("imageLinks.size =>", String.valueOf(imageLinks.size()));
    }

    @Override
    public int getItemViewType(int position) {
        if (position < imageLinks.size()) {
            if (imageLinks.get(position).getIsAnimated().equals("true")) {
                return VIEW_GIF;
            } else {
                return VIEW_IMAGE;
            }
        } else {
            return VIEW_COMMENT;
        }
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_GIF) {
            View itemView = inflater.inflate(R.layout.custom_row_comments_gif, parent, false);
            return new RvHolderGif(itemView);
        } else if (viewType == VIEW_IMAGE) {
            View itemView = inflater.inflate(R.layout.custom_row_comments_image, parent, false);
            return new RvHolderImage(itemView);
        } else {
            View itemView = inflater.inflate(R.layout.custom_row_comments, parent, false);
            return new RvHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final RvHolder holder, final int position) {
        if (getItemViewType(position) == VIEW_IMAGE) {
            String imageUrl = imageLinks.get(position).getLink();
            Picasso.with(activity).load(imageUrl).into(holder.image);
            String description = imageLinks.get(position).getDescription();
            if (!description.contains("null")) {
                ((RvHolderImage) holder).description.setText(imageLinks.get(position).getDescription());
            } else {
                ((RvHolderImage) holder).description.setVisibility(View.GONE);
            }
        } else if (getItemViewType(position) == VIEW_GIF) {
            String mp4 = imageLinks.get(position).getMp4();
            ((RvHolderGif) holder).player.setCallback(this);
            ((RvHolderGif) holder).player.setSource(Uri.parse(mp4));
            ((RvHolderGif) holder).player.start();
        } else if (getItemViewType(position) == VIEW_COMMENT) {
            final int pos = position - imageLinks.size();
            com = list.get(pos);
            holder.itemView.setTag(pos);
            final String comment = com.getComment();
            if (comment.contains("http") && comment.contains("jpg")) {
                holder.author.setText(com.getAuthor());
                String[] s1 = comment.split("http");
                if (s1[0].equals("")) {
                    holder.comment.setVisibility(View.GONE);
                } else {
                    holder.comment.setText(s1[0]);
                }
                String url1 = "http" + s1[1];
                String[] s2 = url1.split("jpg");
                String url2 = s2[0] + "jpg";
                Picasso.with(activity).load(url2).into(holder.image);
                if (s2.length == 2) {
                    holder.comment2.setText(s2[1]);
                } else {
                    holder.comment2.setVisibility(View.GONE);
                }
            } else if (comment.contains("http") && comment.contains("png")) {
                holder.author.setText(com.getAuthor());
                String[] s1 = comment.split("http");
                if (s1[0].equals("")) {
                    holder.comment.setVisibility(View.GONE);
                } else {
                    holder.comment.setText(s1[0]);
                }
                String url1 = "http" + s1[1];
                String[] s2 = url1.split("png");
                String url2 = s2[0] + "png";
                Picasso.with(activity).load(url2).into(holder.image);
                if (s2.length == 2) {
                    holder.comment2.setText(s2[1]);
                } else {
                    holder.comment2.setVisibility(View.GONE);
                }
            } else if (comment.contains("http") && comment.contains("gif")) {
                holder.author.setText(com.getAuthor());
                String[] s1 = comment.split("http");
                if (s1[0].equals("")) {
                    holder.comment.setVisibility(View.GONE);
                } else {
                    holder.comment.setText(s1[0]);
                }
                String url1 = "http" + s1[1];
                String[] s2 = url1.split("gif");
                final String url2 = s2[0] + "gif";
                Glide.with(activity).load(url2).asGif().into(holder.image);
                /*Glide.with(activity).load(url2).asBitmap().listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        Glide.with(activity).load(url2).asGif().into(holder.image);
                        return false;
                    }
                }).into(holder.image);*/

                if (s2.length == 2) {
                    holder.comment2.setText(s2[1]);
                } else {
                    holder.comment2.setVisibility(View.GONE);
                }
            } else {
                holder.image.setVisibility(View.GONE);
                holder.comment.setText(comment);
                holder.comment2.setVisibility(View.GONE);
                holder.author.setText(com.getAuthor());
            }
            final JSONArray replies = com.getjReplies();
            if (replies.length() > 0) {
                if (replies.length() == 1) {
                    holder.numOfReplies.setText(replies.length() + " reply");
                } else {
                    holder.numOfReplies.setText(replies.length() + " replies");
                }
            } else {
                holder.numOfReplies.setVisibility(View.GONE);
            }
            ViewGroup.MarginLayoutParams marginLayoutParams =
                    (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
            marginLayoutParams.setMargins(com.getMargin() * 50, 5, 5, 5);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer tag = (Integer) v.getTag();
                    CardView card = new CardView(activity);
                    final JSONArray array = com.getjReplies();
                    String reply = "";
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                reply = object.getString("comment");
                                String author = object.getString("author");
                                JSONArray replies2 = object.getJSONArray("children");
                                int margin = com.getMargin();
                                //Comment comment1 = new Comment(reply, replies2, margin+1, author+":", i);
                                //activity.addReplies(card, position);
                                //int margin = com.getMargin();
                                int adapterPosition = holder.getAdapterPosition() + 1;
                                list.add(adapterPosition - 1, new Comment(reply, replies2, margin + 1, author + ":", i));

                                //notifyItemInserted(adapterPosition);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        notifyItemRangeChanged(holder.getAdapterPosition() + 1, array.length());

                    }

                    //notifyItemInserted(tag+2);
                    //notifyDataSetChanged();
                }
            });
        }
/********** Context menu **************************************************************************/
        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
/********** Inflate context menu ******************************************************************/
                MenuInflater inflater = activity.getMenuInflater();
                inflater.inflate(R.menu.comments_context_menu, contextMenu);
/********** Share Image ***************************************************************************/
                MenuItem shareImage = contextMenu.findItem(R.id.share_image_comments_context_menu);
                shareImage.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, imageLinks.get(position).getLink());
                        shareIntent.setType("text/plain");
                        activity.startActivity(Intent.createChooser(shareIntent, "Share image with..."));
                        return true;
                    }
                });
/********** Save Image ****************************************************************************/
                MenuItem saveImage = contextMenu.findItem(R.id.save_image_comments_context_menu);
                saveImage.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Toast.makeText(activity, "Save Image", Toast.LENGTH_SHORT).show();
                        new VolleyLoader(activity).userAction(imageLinks.get(position).getId(), "favoriteImage");
                        return true;
                    }
                });
/********** Download Image ************************************************************************/
                MenuItem downloadImage = contextMenu.findItem(R.id.download_image_comments_context_menu);
                downloadImage.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);

                        Uri downloadUri = Uri.parse("http://i.imgur.com/"+imageLinks.get(position).getId()+".jpg");
                        DownloadManager.Request request = new DownloadManager.Request(
                                downloadUri);

                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        request.setAllowedNetworkTypes(
                                DownloadManager.Request.NETWORK_WIFI
                                        | DownloadManager.Request.NETWORK_MOBILE)
                                .setAllowedOverRoaming(false).setTitle("image")
                                .setDescription("imgur.com");
                        request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS, "download");

                        mgr.enqueue(request);
                        return true;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = list.size() + imageLinks.size();
        return size;
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }
}
