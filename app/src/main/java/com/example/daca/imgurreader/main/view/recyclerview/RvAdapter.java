package com.example.daca.imgurreader.main.view.recyclerview;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.daca.imgurreader.main.MainActivity;
import com.example.daca.imgurreader.main.ObserverBuilder;
import com.example.daca.imgurreader.main.entities.Post;
import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.comments.CommentsActivity;
import com.example.daca.imgurreader.util.VolleyLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Daca on 14.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    private ArrayList<Post> list;
    private MainActivity activity;
    private LayoutInflater inflater;
    private int layout;
    Post post;
    String imageUrl;

    public RvAdapter(ArrayList<Post> list, MainActivity activity, int layout) {
        this.list = list;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
        this.layout = layout;
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(layout, parent, false);
        switch (layout){
            case R.layout.custom_row_main:
                return new RvHolder(itemView);
            case R.layout.custom_row_main_cards2:
                return new RvHolderCards2(itemView);
            case R.layout.custom_row_main_small_card:
                return new RvHolderSmallCards(itemView);
            case R.layout.custom_row_main_list:
                return new RvHolderList(itemView);
            case R.layout.custom_row_main_blank:
                return new RvHolderBlank(itemView);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RvHolder holder, final int position) {
        final Post post = list.get(position);
        final ImageView image = setImage(holder);
        final ProgressBar progressBar = setProgressBar(holder);
        final TextView title  = setTitle(holder);
        final TextView ups    = setUps(holder);
        View itemView   = setItemView(holder);
        holder.author.setText(post.getAuthor());
        String gifCover = post.getGifCover();

        this.post = post;
        imageUrl = "http://i.imgur.com/"+post.getCover()+".jpg";

        if (title != null && image != null && itemView != null) {
            if (post.getCover() != null){
                String sHeight = post.getCoverHeight();
                if (sHeight != null && Integer.valueOf(sHeight) > 3000){
                    //holder.imageView.setMaxHeight(300);
                    Picasso.with(activity).load("http://i.imgur.com/"+post.getCover()+".jpg")
                            .resize(300, 300)
                            .centerCrop()
                            .into(image, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {

                                }
                            });
                } else {
                    Picasso.with(activity).load(imageUrl)
                            .into(image, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {

                                }
                            });
                }

                title.setText(post.getTitle());
            }else if (post.getGif() != null){
                progressBar.setVisibility(View.GONE);
                Glide.with(activity).load(post.getGifCover()).asBitmap().into(image);
                title.setText(post.getTitle());
            } else if (post.getGifCover().contains(".png") || post.getGifCover().contains(".jpg")){
                title.setText(post.getTitle());
                //Glide.with(activity).load(post.getAuthor())
                //        .fitCenter().into(holder.imageView);
                Picasso.with(activity).load(gifCover).into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });
            }else if (post.getGifCover().contains(".gif")){
                progressBar.setVisibility(View.GONE);
                Glide.with(activity).load(post.getGifCover()).asBitmap()
                        /*.fitCenter().listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        Log.d("Glide Error =>", e.toString());
                        list.remove(position);
                        notifyDataSetChanged();
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })*/.into(image);
                title.setText(post.getTitle());
            }


            ups.setText(post.getUps());
            holder.downs.setText(post.getDowns());
            ups.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int upsInt = Integer.valueOf(post.getUps()) + 1;
                    ups.setText(upsInt + " ups");
                    ups.setTextColor(Color.parseColor("#1abc9c"));

                    new VolleyLoader(activity).userAction(post.getId(), "upVote");
                }
            });

            holder.downs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int downsInt = Integer.valueOf(post.getDowns()) - 1;
                    holder.downs.setText(downsInt + " downs");
                    holder.downs.setTextColor(Color.parseColor("#ff0000"));

                    new VolleyLoader(activity).userAction(post.getId(), "downVote");
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CommentsActivity.class);
                    intent.putExtra("title", post.getTitle());
                    intent.putExtra("id", post.getId());
                    activity.startActivity(intent);
                }
            });
        }
        if (post.isFavorite()){
            holder.saved.setImageResource(R.drawable.filled_hearth);
        }

        holder.saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.saved.setImageResource(R.drawable.filled_hearth);
                new VolleyLoader(activity).userAction(post.getId(), "favoriteAlbum");
                /*if (!post.isFavorite()){

                }*/
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(activity, view);
                final Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, post.getTitle());
                popup.getMenuInflater().inflate(R.menu.share_popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.share_image_share_popup_menu:
                                shareIntent.putExtra(Intent.EXTRA_TEXT, "http://i.imgur.com/"+post.getCover()+".jpg");
                                shareIntent.setType("text/plain");
                                activity.startActivity(Intent.createChooser(shareIntent, "Share image with..."));
                                break;
                            case R.id.share_album_share_popup_menu:
                                shareIntent.putExtra(Intent.EXTRA_TEXT, "http://imgur.com/gallery/" + post.getId());
                                shareIntent.setType("text/plain");
                                activity.startActivity(Intent.createChooser(shareIntent, "Share image with..."));
                                break;
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(activity, view);
                popup.getMenuInflater().inflate(R.menu.overflow_popup_menu, popup.getMenu());
                final String author = post.getAuthor().substring(3);
                popup.getMenu().add(0, 15, 3, "View " + author + " profile");
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_1_overflow_popup_menu:

                                DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);

                                Uri downloadUri = Uri.parse("http://i.imgur.com/"+post.getCover()+".jpg");
                                DownloadManager.Request request = new DownloadManager.Request(
                                        downloadUri);

                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                request.setAllowedNetworkTypes(
                                        DownloadManager.Request.NETWORK_WIFI
                                                | DownloadManager.Request.NETWORK_MOBILE)
                                        .setAllowedOverRoaming(false).setTitle(post.getTitle())
                                        .setDescription("imgur.com");
                                request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS, "download");

                                mgr.enqueue(request);
                                break;
                            case 15:
                                new VolleyLoader(activity).loadWithToken(
                                        new ObserverBuilder(activity).getPopularObserver(),
                                        //"https://api.imgur.com/3/account/"+author+"/favorites/0/",
                                        //"https://api.imgur.com/3/account/"+author+"/settings",
                                        "https://api.imgur.com/3/account/"+author+"/albums/0",
                                        "loadPopular",
                                        false
                                );
                                break;
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private View setItemView(RvHolder holder) {
        switch (layout){
            case R.layout.custom_row_main:
                return holder.itemView;
            case R.layout.custom_row_main_cards2:
                return  ((RvHolderCards2) holder).itemView;
            case R.layout.custom_row_main_small_card:
                return ((RvHolderSmallCards) holder).itemView;
            case R.layout.custom_row_main_list:
                return ((RvHolderList) holder).itemView;
            case R.layout.custom_row_main_blank:
                return ((RvHolderBlank) holder).itemView;
            default:
                return null;
        }
    }

    private TextView setTitle(RvHolder holder) {
        switch (layout){
            case R.layout.custom_row_main:
                return holder.title;
            case R.layout.custom_row_main_cards2:
                return  ((RvHolderCards2) holder).title2;
            case R.layout.custom_row_main_small_card:
                return ((RvHolderSmallCards) holder).title;
            case R.layout.custom_row_main_list:
                return ((RvHolderList) holder).title;
            case R.layout.custom_row_main_blank:
                return ((RvHolderBlank) holder).title;
            default:
                return null;
        }
    }

    private TextView setUps(RvHolder holder) {
        switch (layout){
            case R.layout.custom_row_main:
                return holder.ups;
            case R.layout.custom_row_main_cards2:
                return  ((RvHolderCards2) holder).ups2;
            case R.layout.custom_row_main_small_card:
                return ((RvHolderSmallCards) holder).ups;
            case R.layout.custom_row_main_list:
                return ((RvHolderList) holder).ups;
            case R.layout.custom_row_main_blank:
                return ((RvHolderBlank) holder).ups;
            default:
                return null;
        }
    }

    private ImageView setImage(RvHolder holder) {
        switch (layout){
            case R.layout.custom_row_main:
                return holder.imageView;
            case R.layout.custom_row_main_cards2:
                return  ((RvHolderCards2) holder).imageView2;
            case R.layout.custom_row_main_small_card:
                return ((RvHolderSmallCards) holder).imageView;
            case R.layout.custom_row_main_list:
                return ((RvHolderList) holder).imageView;
            case R.layout.custom_row_main_blank:
                return ((RvHolderBlank) holder).imageView;
            default:
                return null;
        }
    }

    private ProgressBar setProgressBar(RvHolder holder) {
        switch (layout) {
            case R.layout.custom_row_main:
                return holder.progressBar;
            case R.layout.custom_row_main_cards2:
                return ((RvHolderCards2) holder).progressBar;
            case R.layout.custom_row_main_small_card:
                return ((RvHolderSmallCards) holder).progressBar;
            case R.layout.custom_row_main_list:
                return ((RvHolderList) holder).progressBar;
            case R.layout.custom_row_main_blank:
                return ((RvHolderBlank) holder).progressBar;
            default:
                return null;
        }
    }

    /*
    @Override
    public void onClick(View view) {
        PopupMenu popup = new PopupMenu(activity, view);
        final Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, post.getTitle());
        switch (view.getTag().toString()){
            case "share":

                break;

            case "overflow":
                popup.getMenuInflater().inflate(R.menu.overflow_popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_1_overflow_popup_menu:

                                break;
                        }

                        return true;
                    }
                });
                popup.show();
                break;
        }
    }*/
}
