package com.example.daca.imgurreader.main.view.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 19.04.2017..
 */

public class RvHolderCards2 extends RvHolder {

    TextView title2;
    TextView ups2;
    ImageView imageView2;
    ProgressBar progressBar;

    public RvHolderCards2(View itemView) {
        super(itemView);
        title2 = (TextView) itemView.findViewById(R.id.txt_title_custom_row_main_cards2);
        ups2 = (TextView) itemView.findViewById(R.id.txt_ups_custom_row_main_cards2);
        imageView2 = (ImageView) itemView.findViewById(R.id.image_custom_row_main_cards2);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar_cards2);
    }
}
