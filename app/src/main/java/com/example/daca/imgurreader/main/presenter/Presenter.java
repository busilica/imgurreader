package com.example.daca.imgurreader.main.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.MainActivity;
import com.example.daca.imgurreader.main.ObserverBuilder;
import com.example.daca.imgurreader.util.Url;
import com.example.daca.imgurreader.util.VolleyLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ttlnisoffice on 7/21/17.
 */

public class Presenter {

    private MainActivity activity;
    private VolleyLoader vLoader;
    private ObserverBuilder observerBuilder;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Url url;

    public Presenter(MainActivity activity) {
        this.activity = activity;
        vLoader = new VolleyLoader(activity);
        observerBuilder = new ObserverBuilder(activity);

        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = preferences.edit();

        url = new Url(activity);
    }

/********** SpinnerHelper *************************************************************************/

    public void spinnerHelperItemClick(String selectedItem){

        String url = createUrl(selectedItem);

        editor.putString("url", url);
        editor.apply();

        activity.recyclerHelper.resetRv();

        if (!url.contains("no") && !url.contains("tags")) {
            vLoader.loadWithToken(observerBuilder.getPopularObserver(),
                    url,
                    "loadPopular",
                    shouldRefreshToken());
        }else if (url.contains("tags")){
            vLoader.loadWithToken(observerBuilder.getPopularObserver(),
                    url,
                    "loadTags",
                    shouldRefreshToken());
        }else {
            String isLoggedIn = preferences.getString("isLoggedIn", null);
            if (isLoggedIn != null && isLoggedIn.equals("yes")){
                vLoader.loadWithToken(observerBuilder.getPopularObserver(),
                        "https://api.imgur.com/3/account/me/favorites/0/", "loadPopular",
                        shouldRefreshToken());
            }else {
                Toast.makeText(activity, "Please login!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private String createUrl(String selectedItem) {

        switch (selectedItem) {
            case "Most Viral - popular":
                return url.getViralHot();
            case "User Submitted - popular":
                return url.getUserHot();
            case "Most Viral - newest":
                return url.getViralNew();
            case "User Submitted - newest":
                return url.getUserNew();
            case "Saved":
                return "no";
            case "Tag":
                return "https://api.imgur.com/3/tags";
            default:
                return "no";
        }
    }

    private boolean shouldRefreshToken() {

        String isLoggedIn = preferences.getString("isLoggedIn", null);
        Date now = new Date();
        String expirationDateString = preferences.getString("TOKEN_EXPIRATION", null);

        if (isLoggedIn != null && isLoggedIn.equals("yes") && expirationDateString != null){

            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            Date expirationDate = null;
            try {
                expirationDate = dateFormat.parse(expirationDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (now.compareTo(expirationDate) == -1 /*token != null*/) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }



/********** MainActivity **************************************************************************/

    public void changeTheme(){
        int theme = preferences.getInt("THEME", R.id.theme_light);
        switch (theme){
            case R.id.theme_light:
                activity.setTheme(R.style.ActivityThemeLight);
                break;
            case R.id.theme_dark:
                activity.setTheme(R.style.ActivityThemeDark);
                break;
            case R.id.theme_sol_light:
                activity.setTheme(R.style.ActivityThemeSolarizedLight);
                break;
            case R.id.theme_night_blue:
                activity.setTheme(R.style.ActivityThemeNightBlue);
                break;
        }
    }

    public void onResume(){
        //first if is required to avoid mixing onResume with changing themes
        if (activity.getIntent() != null && activity.getIntent().getAction() != null){
            if (activity.getIntent()!=null && activity.getIntent().getAction().equals(Intent.ACTION_VIEW)){
                Uri uri = activity.getIntent().getData();
                if (uri.getQueryParameter("error")!=null){
                    String error = uri.getQueryParameter("error");
                    Log.e("Error: ", "An eror has occured :" + error);
                }else {

                    String code = uri.toString();

                    //token
                    int start = code.indexOf("=");
                    int end = code.indexOf("&");
                    String token = code.substring(start+1, end);

                    //user
                    int start2 = code.indexOf("=", 195);
                    int end2 = code.indexOf("&", 195);
                    String user = code.substring(start2+1, end2);

                    //refresh token
                    /*int start3 = code.indexOf("=", 135);
                    int end3 = code.indexOf("&", 175);
                    String refreshToken = code.substring(start, end);*/

                    //String s code.split("&");

                    String[] s = code.split("&");

                    String refreshToken = s[3];
                    String refreshToken2 = refreshToken.substring(refreshToken.indexOf("=") + 1, refreshToken.length());

                    editor.putString("TOKEN", token);
                    editor.putString("USER", user);
                    editor.putString("isLoggedIn", "yes");

                    Date now = new Date();
                    now.setMinutes(now.getMinutes() + 30);
                    editor.putString("TOKEN_EXPIRATION", now.toString());

                    editor.putString("REFRESH_TOKEN", refreshToken2);
                    editor.apply();

                }
            }
        }
    }

    public void loginUser(){
        editor.apply(); //TODO Why?
        String urlS = url.getLoginUrl();
        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlS)));
    }

    public void saveTheme(int itemId) {
        editor.putInt("THEME", itemId);
        editor.apply();
    }

    public void restartActivity() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }
}
