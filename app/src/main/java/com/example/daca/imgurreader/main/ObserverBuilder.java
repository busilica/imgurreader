package com.example.daca.imgurreader.main;

import com.example.daca.imgurreader.main.entities.Post;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 14.04.2017..
 */

public class ObserverBuilder {

    private MainActivity activity;

    public ObserverBuilder(MainActivity activity) {
        this.activity = activity;
    }

    public Observer<ArrayList<Post>> getPopularObserver(){
        Observer<ArrayList<Post>> observer = new Observer<ArrayList<Post>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Post> value) {
                activity.recyclerHelper.setupRvAdapter(value);
                activity.recyclerHelper.LoadDrawerItems();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        return observer;
    }

    public Observer<ArrayList<String>> getDrawerObserver(){
        Observer<ArrayList<String>> observer = new Observer<ArrayList<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<String> value) {
               activity.recyclerHelper.setupDrawerAdapter(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        return observer;
    }
}
