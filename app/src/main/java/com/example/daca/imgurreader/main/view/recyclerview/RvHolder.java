package com.example.daca.imgurreader.main.view.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 14.04.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView title;
    TextView author;
    TextView ups;
    TextView downs;
    ImageButton share;
    ImageButton saved;
    ImageButton overflow;
    ImageView imageView;
    ProgressBar progressBar;

    public RvHolder(View itemView) {
        super(itemView);
        title       = (TextView) itemView.findViewById(R.id.txt_title_custom_row_main);
        author      = (TextView) itemView.findViewById(R.id.txt_author_custom_row_main);
        ups         = (TextView) itemView.findViewById(R.id.txt_ups_custom_row);
        downs       = (TextView) itemView.findViewById(R.id.txt_downs_custom_row);
        saved       = (ImageButton) itemView.findViewById(R.id.txt_save_custom_row_main);
        share       = (ImageButton) itemView.findViewById(R.id.txt_share_custom_row_main);
        overflow    = (ImageButton) itemView.findViewById(R.id.txt_overflow_custom_row_main);
        imageView   = (ImageView) itemView.findViewById(R.id.image_custom_row_main);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar_cards);
    }
}
