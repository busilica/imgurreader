package com.example.daca.imgurreader.comments.recyclerview;

import android.view.View;

import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.example.daca.imgurreader.R;

/**
 * Created by Daca on 18.04.2017..
 */

public class RvHolderGif extends RvHolder {

    EasyVideoPlayer player;

    public RvHolderGif(View itemView) {
        super(itemView);
        player = (EasyVideoPlayer) itemView.findViewById(R.id.player);
    }
}
