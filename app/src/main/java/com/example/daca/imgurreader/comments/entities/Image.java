package com.example.daca.imgurreader.comments.entities;

/**
 * Created by Daca on 18.04.2017..
 */

public class Image {

    private String id;
    private String link;
    private String description;
    private String isAnimated;
    private String mp4;

    public Image(){

    }

    public Image(String id, String link, String description, String isAnimated, String mp4) {
        this.link = link;
        this.description = description;
        this.isAnimated = isAnimated;
        this.mp4 = mp4;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsAnimated() {
        return isAnimated;
    }

    public void setIsAnimated(String isAnimated) {
        this.isAnimated = isAnimated;
    }

    public String getMp4() {
        return mp4;
    }

    public void setMp4(String mp4) {
        this.mp4 = mp4;
    }
}
