package com.example.daca.imgurreader.util;

import android.util.Log;

import com.example.daca.imgurreader.comments.entities.Image;
import com.example.daca.imgurreader.main.entities.Post;
import com.example.daca.imgurreader.comments.entities.Comment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Daca on 14.04.2017..
 */

public class JsonParser {

    public ArrayList<Post> parsePopular(String data){
        ArrayList<Post> list = new ArrayList<>();
        int index = 0;

        try {
            JSONObject object1 = new JSONObject(data);
            JSONArray array1 = object1.getJSONArray("data");
            for (int i = 0; i < array1.length(); i++){
                index = i;
                JSONObject object2 = array1.getJSONObject(i);
                String id = object2.getString("id");
                String title = object2.getString("title");
                String author = "by " + object2.getString("account_url");
                String cover = "";
                if (object2.has("cover")){
                    cover = object2.getString("cover");
                }else{
                    cover = null;
                }
                String gifCover = object2.getString("link");
                String gif = "";
                if (object2.has("mp4")){
                    gif = object2.getString("mp4");
                }else {
                    gif = null;
                }
                String ups = "";
                if (object2.has("ups")){
                    ups = object2.getString("ups");
                }
                String downs = "";
                if (object2.has("downs")){
                    downs = object2.getString("downs");
                }
                boolean favorite = false;
                if (object2.has("favorite")){
                    favorite = object2.getBoolean("favorite");
                }
                String height = null;
                //if (object2.has("is_album") && object2.getString("is_album").equals("true")){

                    if (object2.has("cover_height")){
                        if (!object2.getString("cover_height").contains("null")){
                            height = object2.getString("cover_height");
                            String heightS = object2.getString("cover_height");
                            int intHeight = Integer.valueOf(object2.getString("cover_height"));
                            if (intHeight < 5100){
                                list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));
                            }
                        }else {
                            list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));

                        }
                    }else {
                        list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));
                    }
                //}
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("parsePosts => ", String.valueOf(index));
            Log.d("parsePosts => ", e.toString());
        }

        return list;
    }

    public ArrayList<Comment> parseComments(String data) {
        ArrayList<Comment> list = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data);
            JSONArray array1 = object1.getJSONArray("data");
            for (int i = 0; i < array1.length(); i++){
                JSONObject object2 = array1.getJSONObject(i);
                String comment = object2.getString("comment");
                JSONArray array2 = object2.getJSONArray("children");
                String author = object2.getString("author");
                list.add(new Comment(comment, array2, 0, author+":", i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("Error, parseComments =>", e.toString());
        }
        String test1 = "";
        Log.d("Success, parseCom =>", String.valueOf(list.size()));
        return list;
    }

    public ArrayList<Image> parseImages(String data) {
        ArrayList<Image> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(data);
            JSONArray array1 = object.getJSONArray("data");
            for (int i = 0; i < array1.length(); i++){
                JSONObject object2 = array1.getJSONObject(i);
                String link = object2.getString("link");
                String description = object2.getString("description");
                String isAnimated = object2.getString("animated");
                String id = object2.getString("id");
                String mp4 = "";
                if (object2.has("mp4")){
                   mp4 = object2.getString("mp4");
                }else {
                    mp4 = null;
                }
                list.add(new Image(id, link, description, isAnimated, mp4));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("Error, parseImages =>", e.toString());
        }
        return list;
    }

    public ArrayList parseTags(String data) {
        ArrayList<String> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(data);
            JSONObject object2 = object.getJSONObject("data");
            JSONArray array1 = object2.getJSONArray("tags");
            for (int i = 0; i < 30; i++){
                JSONObject object3 = array1.getJSONObject(i);
                String tag = object3.getString("name");
                list.add(tag);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList parseTagItems(String data){
        ArrayList<Post> list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(data);
            JSONObject objectD = object.getJSONObject("data");
            JSONArray items = objectD.getJSONArray("items");
            for (int i = 0; i < items.length(); i++){

                JSONObject object2 = items.getJSONObject(i);
                String id = object2.getString("id");
                String title = object2.getString("title");
                String author = "by " + object2.getString("account_url");
                String cover = "";
                if (object2.has("cover")){
                    cover = object2.getString("cover");
                }else{
                    cover = null;
                }
                String gifCover = object2.getString("link");
                String gif = "";
                if (object2.has("mp4")){
                    gif = object2.getString("mp4");
                }else {
                    gif = null;
                }
                String ups = "";
                if (object2.has("ups")){
                    ups = object2.getString("ups");
                }
                String downs = "";
                if (object2.has("downs")){
                    downs = object2.getString("downs");
                }
                boolean favorite = false;
                if (object2.has("favorite")){
                    favorite = object2.getBoolean("favorite");
                }
                String height = null;
                //if (object2.has("is_album") && object2.getString("is_album").equals("true")){

                if (object2.has("cover_height")){
                    if (!object2.getString("cover_height").contains("null")){
                        height = object2.getString("cover_height");
                        String heightS = object2.getString("cover_height");
                        int intHeight = Integer.valueOf(object2.getString("cover_height"));
                        if (intHeight < 5100){
                            list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));
                        }
                    }else {
                        list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));

                    }
                }else {
                    list.add(new Post(id, title, author, cover, gifCover, gif, ups, downs, height, favorite));
                }
                //}
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}
