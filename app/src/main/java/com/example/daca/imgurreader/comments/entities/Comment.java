package com.example.daca.imgurreader.comments.entities;

import org.json.JSONArray;

/**
 * Created by Daca on 16.04.2017..
 */

public class Comment {

    private String comment;
    private JSONArray jReplies;
    private int margin;
    private String author;
    private int commentPosition;

    public Comment(String comment, JSONArray jReplies, int margin, String author, int commentPosition) {
        this.comment = comment;
        this.jReplies = jReplies;
        this.margin = margin;
        this.author = author;
        this.commentPosition = commentPosition;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public JSONArray getjReplies() {
        return jReplies;
    }

    public void setjReplies(JSONArray jReplies) {
        this.jReplies = jReplies;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCommentPosition() {
        return commentPosition;
    }

    public void setCommentPosition(int commentPosition) {
        this.commentPosition = commentPosition;
    }
}
