package com.example.daca.imgurreader.main.view.drawer_rv;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.MainActivity;
import com.example.daca.imgurreader.main.ObserverBuilder;
import com.example.daca.imgurreader.util.VolleyLoader;

import java.util.ArrayList;

/**
 * Created by ttlnisoffice on 8/22/17.
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    MainActivity activity;
    ArrayList<String> list;
    LayoutInflater inflater;

    public RvAdapter(MainActivity activity, ArrayList<String> list) {
        this.activity = activity;
        this.list = list;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_main_drawer_custom_row, parent, false);
        return new RvHolder(view);
    }

    @Override
    public void onBindViewHolder(RvHolder holder, int position) {
        final String tag = list.get(position);
        holder.tag.setText(tag);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new VolleyLoader(activity).loadWithToken(
                        new ObserverBuilder(activity).getPopularObserver(),
                        "https://api.imgur.com/3/gallery/t/"+tag+"/viral/day/0",
                        "loadTagItems",
                        false);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
