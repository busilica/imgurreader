package com.example.daca.imgurreader.main.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.daca.imgurreader.R;
import com.example.daca.imgurreader.main.presenter.Presenter;
import com.example.daca.imgurreader.main.view.recyclerview.RvAdapter;
import com.example.daca.imgurreader.util.LayoutCollectons;

/**
 * Created by Daca on 22.04.2017..
 */

/*
 *   Not activity,
 *   handles onCreateOptionsMenu and onOptionsItemSelected
 */
public abstract class OptionsActivity extends AppCompatActivity {

    Presenter presenter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        presenter = getPresenter();

        if (item.getItemId() == R.id.log_in){
            presenter.loginUser();
        }

        if (item.getGroupId() == R.id.theme_group){
            presenter.saveTheme(item.getItemId());
            presenter.changeTheme();
            changeTheme();
        }


        /*switch (item.getItemId()){

            case R.id.log_in:
                presenter.loginUser();
                break;
            /*case R.id.cards:
                item.setChecked(true);
                changeLayout(LayoutCollectons.getCARDS());
                editor.putInt("LAYOUT", 0);
                editor.apply();
                break;
            case R.id.cards2:
                item.setChecked(true);
                changeLayout(LayoutCollectons.getCARDS2());
                editor.putInt("LAYOUT", 1);
                editor.apply();
                break;
            case R.id.small_cards:
                item.setChecked(true);
                changeLayout(LayoutCollectons.getSMALLCARDS());
                editor.putInt("LAYOUT", 2);
                editor.apply();
                break;
            case R.id.list:
                item.setChecked(true);
                changeLayout(LayoutCollectons.getLIST());
                editor.putInt("LAYOUT", 3);
                editor.apply();
                break;
            case R.id.blank:
                item.setChecked(true);
                changeLayout(LayoutCollectons.getBLANK());
                editor.putInt("LAYOUT", 4);
                editor.apply();
                break;*/
            /*case R.id.theme_light:
                //setTheme(R.style.ActivityThemeLight);
                changeTheme();
                editor.putInt("THEME", 0);
                editor.apply();
                break;
            case R.id.theme_dark:
                //setTheme(R.style.ActivityThemeDark);
                changeTheme();
                editor.putInt("THEME", 1);
                editor.apply();
                break;
            case R.id.theme_sol_light:
                //setTheme(R.style.ActivityThemeSolarizedLight);
                changeTheme();
                editor.putInt("THEME", 2);
                editor.apply();
                break;
            case R.id.theme_night_blue:
                //setTheme(R.style.ActivityThemeNightBlue);
                changeTheme();
                editor.putInt("THEME", 3);
                editor.apply();
                break;
        }*/
        return super.onOptionsItemSelected(item);
    }

    public abstract void changeLayout(int layout);

    public abstract void changeTheme();

    public abstract Presenter getPresenter();
}
